var app = angular.module('frequency_capping', []);

// TODO: Need to place it in filters folder & should be more generic
app.filter('findLineItem', function() {
  return function(groups, line_item_id) {
    var i=0, len=groups.length;
    for (; i<len; i++) {
      if (+groups[i].line_items.indexOf(line_item_id) > -1) {
        return i;
      }
    }
    return null;
  }
});

function isEmpty(value){
  return (value == null || value=="")
};

app.controller('FrequencyCappingController', function($scope, findLineItemFilter) {

    $scope.allCheck = false;

    // Need to be more generic
    $scope.validation_messages = {
        view_count: "Please enter a positive whole number for Time Based Views",
        time_based: "You must define valid Time-Base.",
        day_range: "Please enter a non-negative whole number for days",
        hours_range: "Please enter a whole number (0 - 23) for hours",
        minutes_range: "Please enter a whole number (0 - 59) for minutes",
        line_items: "Select at least one line item",
        unused_groups: "Remove unused groups before saving"
    }


    // Test data. TODO - remove after testing
    $scope.io = {
        line_items: [ {id: 1, text: "ToolTip for id 1"},
                      {id: 2, text: "ToolTip for id 2"},
                      {id: 3, text: "ToolTip for id 3"}],

        frequency_groups: [
          {
            view_count: 1,
            days: 0,
            hours:0,
            minutes:0,
            line_items: []
          },
          {
            view_count: 2,
            days: 5,
            hours:2,
            minutes:30,
            line_items: []
          }
        ]
    };


    // Insert frequency group to IO
    $scope.addItem = function() {

        if ($scope.allCheck){
         return false;
        }

        $scope.io.frequency_groups.push(
          {
            view_count: null,
            days: 0,
            hours:0,
            minutes:30,
            line_items: []
          }
        );
    },

    // Remove given frequency group from IO
    $scope.removeItem = function(index) {
        $scope.io.frequency_groups.splice(index, 1);
    },

    // Add or Remove line item id to the frequency group
    $scope.modifyLineItem = function($event, frequency_group_index, line_item) {
        var ele = $event.target;
        var line_items = $scope.io.frequency_groups[frequency_group_index].line_items	;
        if(ele.checked){
          line_items.push(line_item);
        } else {
          line_items.splice(line_items.indexOf(line_item), 1);
        }
    },

    // Check presence of line item id for given group
    $scope.isPresenceOf = function(f_group, line_item){
        var ids = f_group.line_items;
        return ids.indexOf(line_item) > -1;
    }

    // Used to disable line item in a group
    $scope.isDisabled = function(f_group, line_item) {
        var disable = false;
        var frequency_group_index = findLineItemFilter($scope.io.frequency_groups, line_item);
        if (frequency_group_index != null){
          if (frequency_group_index == f_group){
            disable = false;
          } else {
            disable = true;
          }
        }
        return ($scope.allCheck || disable);
    }

    // All check box conditions
    $scope.allCheckCondition = function(){
        var frequency_groups = $scope.io.frequency_groups;


            for (i=0; i < frequency_groups.length; i++) {
              if ($scope.allCheck && i==0){
                  var io_line_item_ids = $scope.io.line_items.map(function(p) { return p.id;});
                  frequency_groups[i].line_items = io_line_item_ids;
              }else {
                frequency_groups[i].line_items = [];
              }
            }

    }

    // Check empty line items in a group
    $scope.isLineItemEmpty = function(group_index){
        var frequency_group_capping = $scope.io.frequency_groups[group_index];
        return frequency_group_capping.line_items.length == 0;
    }


    // check to show error messages on label
    $scope.hasError = function(form, index, field){

        if (!(form.$submitted)){
            return false;
        }
        if (($scope.allCheck && index!=0) || ($scope.isAllLineItemsSelected() && $scope.isLineItemEmpty(index))) {
            return false;
        } else {
            if (field == "line_items"){
                return   $scope.isLineItemEmpty(index)
            } else {
                //TODO: Need to find alternate for below line
                var ngFormName = angular.element('ng-form')[index].attributes[1].nodeValue;
                return (form[ngFormName] ? form[ngFormName][field].$invalid : false)
            }
        }

    }

    // check if any unused groups
    $scope.hasUnusedGroup = function(){
        var frequency_groups = $scope.io.frequency_groups;

        if (!$scope.isAllLineItemsSelected())
            return false;


        for (i=0; i < frequency_groups.length; i++) {
            if (frequency_groups[i].line_items.length==0){
                return true;
            }
        }

        return false;
    }

    // Check whether all line items are checked in a groups
    $scope.isAllLineItemsSelected = function(){
        var frequency_groups = $scope.io.frequency_groups;

        var allSelectedLineItems = []


        for (i=0; i < frequency_groups.length; i++) {
            if (frequency_groups[i].line_items){
                allSelectedLineItems.push(frequency_groups[i].line_items)
            }
        }

        var flattenItems = Array.prototype.concat.apply([], allSelectedLineItems);
        return angular.equals(flattenItems.sort(), $scope.io.line_items.map(function(p) { return p.id;}).sort())
    }

    // Display alert messages on submit
    $scope.alertValidationMessage = function(form){
        var validation_messages = [];

        var i=0, len=$scope.io.frequency_groups.length;

        if ($scope.hasUnusedGroup()) {

            validation_messages.push($scope.validation_messages.unused_groups)

        } else {


            for (; i<len; i++) {
                //TODO: Need to find alternate for below line
                var ngFormName = angular.element('ng-form')[i].attributes[1].nodeValue
                var currentNestedForm = form[ngFormName];

                if (currentNestedForm.$invalid){
                    var hasTimeBasedFormatError = false;
                    if(currentNestedForm.view_count.$invalid){
                        if (validation_messages.indexOf($scope.validation_messages.view_count) == -1){
                            validation_messages.push($scope.validation_messages.view_count);
                        }
                    }

                    if (currentNestedForm.days.$error.max || currentNestedForm.days.$error.number || currentNestedForm.days.$error.pattern){
                        hasTimeBasedFormatError = true;
                        if (validation_messages.indexOf($scope.validation_messages.day_range) == -1){
                            validation_messages.push($scope.validation_messages.day_range);
                        }
                    }


                    if (currentNestedForm.hours.$error.max || currentNestedForm.hours.$error.number || currentNestedForm.hours.$error.pattern){
                        hasTimeBasedFormatError = true;
                        if (validation_messages.indexOf($scope.validation_messages.hours_range) == -1){

                            validation_messages.push($scope.validation_messages.hours_range);
                        }
                    }


                    if (currentNestedForm.minutes.$error.max || currentNestedForm.minutes.$error.number || currentNestedForm.minutes.$error.pattern){
                        hasTimeBasedFormatError = true;
                        if (validation_messages.indexOf($scope.validation_messages.minutes_range) == -1){
                            validation_messages.push($scope.validation_messages.minutes_range);
                        }
                    }


                     if (!hasTimeBasedFormatError && (currentNestedForm.days.$error.share ||
                         currentNestedForm.hours.$error.share || currentNestedForm.minutes.$error.share)){
                        if (validation_messages.indexOf($scope.validation_messages.time_based) == -1){
                            validation_messages.push($scope.validation_messages.time_based);
                        }
                     }

                    if(currentNestedForm.line_items.$invalid){
                        if (validation_messages.indexOf($scope.validation_messages.line_items) == -1){
                            validation_messages.push($scope.validation_messages.line_items);
                        }
                    }
                }
            }
        }

        alert(validation_messages.join('\n'));

    }

    // on Success submit
    $scope.submitForm = function(){
        alert("submitting form...");
    }

});



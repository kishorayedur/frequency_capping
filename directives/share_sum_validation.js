var app = angular.module('share_sum_validation', []);

// Used to validate multiple fields sum.
// Accepted params {min_count:1,
//                  fields:['days', 'hours', 'minutes', etc.,],
//                  form_name:'formName' otherwise default form}"

app.directive('shareSumValidate', function () {
    return {
        restrict: 'A',
        require:  'ngModel',
        link: function(scope, elem, attr, ctrl) {
            var validateSum = function(value) {
                var params = angular.copy(scope.$eval(attr.shareSumValidate))
                //TODO: Need to find alternate for below line
                var ngFormName = angular.element('ng-form')[params.form_index].attributes[1].nodeValue;

                var current_form =  scope.form[ngFormName] || scope.form;

                var sum = 0;
                angular.forEach(params.fields, function(field){

                    field_value = current_form[field].$viewValue || current_form[field].$modelValue
                    sum += field_value  ? parseInt(field_value) : 0;
                })

                angular.forEach(params.fields, function(field){
                    current_form[field].$setValidity('share', (sum >= params.min_count));
                })
                return value;
            };

            ctrl.$parsers.unshift(validateSum);
            ctrl.$formatters.unshift(validateSum);


        }
    };
});
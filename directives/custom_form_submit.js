var app = angular.module('custom_form_submit', []);

// Call on-submit method only if form is valid otherwise call alertValidationMessage
app.directive('validSubmit', [ '$parse', function($parse) {
    return {
        require: 'form',
        link: function(scope, element, iAttrs, form) {
            form.$submitted = false;
            var fn = $parse(iAttrs.validSubmit);
            element.on('submit', function(event) {
                scope.$apply(function() {
                    form.$submitted = true;
                    if (form.$valid) {
                        fn(scope, { $event : event });
                        form.$submitted = false;
                    } else if(form.$invalid){
                        scope.alertValidationMessage(form);
                    }
                });
            });
        }
    };
}
]);

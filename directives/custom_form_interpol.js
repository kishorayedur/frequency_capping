var app = angular.module('custom_form_interpol', []);

// To get interpolation functionality in ng-form.
app.directive('interpolNgForm', function($interpolate) {
    return {
        restrict: 'AC',
        priority: 10000,
        controller: function($scope, $element, $attrs) {
            $attrs.$set('name', $interpolate($attrs.interpolNgForm || $attrs.name)($scope));
            $element.data('$interpolFormController', null);
        }
    };
})
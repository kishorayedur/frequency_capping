'use strict';

//TODO: Need to find a way to assert validation messages.
// TODO: Mock initial scope.io value

describe('Frequency capping Add/Remove scenerios', function() {


    beforeEach(function() {
        browser().navigateTo('../../index.html');
    });


    it ( 'should add group if user clicked add', function ()
    {
        expect ( repeater ( 'ng-form' ).count () ).toEqual ( 2 );
        element('button[type="button"]').click();
        expect ( repeater ( 'ng-form' ).count () ).toEqual ( 3 );

    } );

    it('Remove group if user click on delete', function(){
        expect ( repeater ( 'ng-form' ).count () ).toEqual ( 2 );
        confirmOK()
        element('a:first').click();
        expect ( repeater ( 'ng-form' ).count () ).toEqual ( 1 );
        element('a:first').click();
    })


    it('should show no frequency capping content', function(){

        expect(element('div.ng-hide').html()).toContain('No Frequency Capping set');
        confirmOK()
        element('a').query(function($el, done) {
            $el.click();
            done();
        });
        expect(element('body').html()).toContain('No Frequency Capping set');
    })
});



describe('Frequency capping validations', function() {

    beforeEach(function() {
        browser().navigateTo('../../index.html');
    });

    it ( 'should show unwanted group error', function ()
    {
        alertOK()
        input('allCheck').check()
        element('button[type="submit"]').click();
        expect( element('div.unwantedGroup').count()).toBe(1);
    });

    it ( 'should show view count error', function ()
    {

        alertOK()
        confirmOK()
        input('allCheck').check()
        element('a:last').click();
        expect ( repeater ( 'ng-form' ).count () ).toEqual ( 1 );

        input('frequency_group.view_count').enter(-1)
        input('frequency_group.days').enter(0)
        input('frequency_group.hours').enter(1)
        input('frequency_group.minutes').enter(0)
        element('button[type="submit"]').click();

        expect(element('span.error').html()).toContain('View Count')

        input('frequency_group.view_count').enter(1)
        expect(element('span.error').count()).toBe(0)
    });


    it ( 'should show time based min error', function ()
    {


        confirmOK()
        alertOK()
        input('allCheck').check()
        element('a:last').click();
        expect ( repeater ( 'ng-form' ).count () ).toEqual ( 1 );
        input('frequency_group.view_count').enter(1)
        input('frequency_group.days').enter(0)
        input('frequency_group.hours').enter(-1)
        input('frequency_group.minutes').enter(0)
        element('button[type="submit"]').click();

        expect(element('span.error').count()).toBe(3)
        expect(element('input.ng-invalid-share').count()).toBe(3)

        input('frequency_group.minutes').enter(2)

        expect(element('span.error').count()).toBe(1)
        expect(element('input[ng-model="frequency_group.hours"].ng-invalid-pattern').count()).toBe(1)
        expect(element('input.ng-invalid-share').count()).toBe(0)
    });


    it ( 'should show time based individual error', function ()
    {

        alertOK()
        confirmOK()
        input('allCheck').check()
        element('a:last').click();
        expect ( repeater ( 'ng-form' ).count () ).toEqual ( 1 );
        input('frequency_group.view_count').enter(1)
        element('button[type="submit"]').click();

        // For pattern mis-match error
        input('frequency_group.days').enter(-1)
        input('frequency_group.hours').enter(-1)
        input('frequency_group.minutes').enter(-1)
        expect(element('input[ng-model="frequency_group.days"].ng-invalid-pattern').count()).toBe(1)
        expect(element('input[ng-model="frequency_group.hours"].ng-invalid-pattern').count()).toBe(1)
        expect(element('input[ng-model="frequency_group.minutes"].ng-invalid-pattern').count()).toBe(1)

        // For max error
        input('frequency_group.hours').enter(25)
        input('frequency_group.minutes').enter(60)
        expect(element('input[ng-model="frequency_group.hours"].ng-invalid-max').count()).toBe(1)
        expect(element('input[ng-model="frequency_group.minutes"].ng-invalid-max').count()).toBe(1)

    });

    it ('should show line items required error', function(){
        alertOK()
        confirmOK()
        element('a:last').click();
        expect ( repeater ( 'ng-form' ).count () ).toEqual ( 1 );
        input('frequency_group.view_count').enter(1)
        input('frequency_group.days').enter(0)
        input('frequency_group.hours').enter(1)
        input('frequency_group.minutes').enter(0)
        element('button[type="submit"]').click();
        expect(element('span.error').html()).toContain('Line Items :')


    })
});


describe('Frequency capping Success submit', function() {

    beforeEach(function() {
        browser().navigateTo('../../index.html');
    });

    it ( 'should not show error if all fields are valid', function (){
        alertOK()
        confirmOK()
        input('allCheck').check()
        element('a:last').click();
        expect ( repeater ( 'ng-form' ).count () ).toEqual ( 1 );
        input('frequency_group.view_count').enter(1)
        input('frequency_group.days').enter(0)
        input('frequency_group.hours').enter(1)
        input('frequency_group.minutes').enter(0)
        element('button[type="submit"]').click();

        expect(element('.error').count()).toBe(0)
    })

});
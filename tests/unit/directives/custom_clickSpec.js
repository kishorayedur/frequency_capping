
describe('customClick', function () {
    var elm, $scope, windowMock;
    beforeEach(module('customClick'));

    beforeEach(inject(function($rootScope, $compile, $window) {
        $scope = $rootScope.$new();
        elm = angular.element('<a href="#" confirmed-click="testConfirm" ng-confirm-click="Are you sure want to delete this capping group"></a>');
        $compile(elm)($scope);
        $scope.$digest();
        windowMock=$window;
    }));


    it('should call confirm action and confirm-click action', function() {
        spyOn(windowMock, "confirm").andReturn(true);
        spyOn($scope, "$eval");
        elm[0].click();
        expect(windowMock.confirm).toHaveBeenCalled();
        expect($scope.$eval).toHaveBeenCalledWith('testConfirm');
    });

    it('should call confirm action and should not call confirm-click action', function() {
        spyOn(windowMock, "confirm").andReturn(false);
        spyOn($scope, "$eval");
        elm[0].click();
        expect(windowMock.confirm).toHaveBeenCalled();
        expect($scope.$eval).not.toHaveBeenCalledWith('testConfirm');
    });

});


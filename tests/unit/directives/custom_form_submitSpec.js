
describe('custom_form_submit', function () {
    var elm, $scope,  submitButton;
    beforeEach(module('custom_form_submit'));

    beforeEach(inject(function($rootScope, $compile, $parse) {
        $scope = $rootScope.$new();
        $scope.submitForm = function(){
           return "success"
        }

        $scope.alertValidationMessage = function(form){
            return "failed"
        }

        submitButton = angular.element('<button type="submit"  class="btn btn-primary btn-sm"  >Save</button>');
        elm = angular.element('<form name="form"  valid-submit="submitForm()" novalidate>')
        elm.append(submitButton)
        elm.append(angular.element('</form>'))
        $compile(elm)($scope);
        $scope.$digest();

        spyOn($scope, 'submitForm')
        spyOn($scope, 'alertValidationMessage')
    }));

    it('should call submit action', function() {
        expect($scope.form.$submitted).toBe(false);
        elm.triggerHandler('submit');
        expect($scope.form.$submitted).toBe(false);
        expect($scope.submitForm).toHaveBeenCalled();
        expect($scope.alertValidationMessage).not.toHaveBeenCalled();
    });

    it('should call invalid method', function() {
        expect($scope.form.$submitted).toBe(false);
        $scope.form.$valid = false;
        $scope.form.$invalid = true;
        elm.triggerHandler('submit');
        expect($scope.form.$submitted).toBe(true);
        expect($scope.submitForm).not.toHaveBeenCalled();
        expect($scope.alertValidationMessage).toHaveBeenCalledWith($scope.form);
    });


});
// TODO: Mock initial scope.io value
describe('frequencyCapping', function () {
    beforeEach(function () {
        module('frequency_capping');
    });
    it('has a findLineItem filter', inject(function($filter) {
        expect($filter('findLineItem')).not.toBeNull();
    }));
});

describe('FrequencyCappingController', function() {
    var scope, ctrl;

    beforeEach(function () {
        module('frequency_capping');
    });

    beforeEach(inject(function( $rootScope, $controller) {

        scope = $rootScope.$new();
        ctrl = $controller('FrequencyCappingController', {$scope: scope});
    }));

    it('should have required objects initialized', function() {
        expect(scope.allCheck).toBeFalsy()
        expect(scope.validation_messages).not.toBeNull();
        expect(scope.io.line_items.length).toEqual(3);
        expect(scope.io.frequency_groups.length).toEqual(2);
    })

    it('should add frequency group item to io', function(){
        expect(scope.io.frequency_groups.length).toEqual(2);
        scope.addItem()
        expect(scope.io.frequency_groups.length).toEqual(3);
    })

    it('should remove frequency group item from io', function(){
        previous_groups = scope.io.frequency_groups.length
        scope.removeItem(0)
        expect(scope.io.frequency_groups.length).toEqual(previous_groups-1);
    })

    it('should return true if given line_item presence in a group', function(){
        expect(scope.isPresenceOf(scope.io.frequency_groups[0], scope.io.line_items[0].id)).toBeFalsy()
        scope.io.frequency_groups[0].line_items.push(scope.io.line_items[0].id)
        expect(scope.isPresenceOf(scope.io.frequency_groups[0], scope.io.line_items[0].id)).toBeTruthy()
    })

    it('should return false is given line items not in any group', function(){
        scope.allCheck = true;
        expect(scope.isDisabled(0, -10)).toBeTruthy()
        expect(scope.isDisabled(1, -10)).toBeTruthy()
    })

    it('should return false if given line_item presence in given group, for other groups it should be false if allCheck="true / false" ', function(){
        scope.allCheck = true;
        scope.io.frequency_groups[0].line_items.push(scope.io.line_items[0].id)
        expect(scope.isDisabled(0, scope.io.line_items[0].id)).toBeTruthy()
        expect(scope.isDisabled(1, scope.io.line_items[0].id)).toBeTruthy()

        scope.allCheck = false;
        expect(scope.isDisabled(0, scope.io.line_items[0].id)).toBeFalsy()
        expect(scope.isDisabled(1, scope.io.line_items[0].id)).toBeTruthy()
    })


    it('should return true if line item is empty in a group',function(){
        scope.io.frequency_groups[0].line_items.push(scope.io.line_items[0].id)
        expect(scope.isLineItemEmpty(0)).toBeFalsy()
        scope.io.frequency_groups[0].line_items = []

        expect(scope.isLineItemEmpty(0)).toBeTruthy()
    })

    it('should define submitForm', function(){
        expect(scope.submitForm).toBeDefined()
    })


    it('should return true if all line items are selected in a groups', function(){
        scope.io.frequency_groups[0].line_items = []
        scope.io.frequency_groups[0].line_items = scope.io.line_items.map(function(p) { return p.id;})

        expect(scope.isAllLineItemsSelected()).toBeTruthy()

        scope.io.frequency_groups[0].line_items = []
        scope.io.frequency_groups[0].line_items.push(scope.io.line_items[0].id)
        scope.io.frequency_groups[1].line_items.push(scope.io.line_items[1].id)

        expect(scope.isAllLineItemsSelected()).toBeFalsy()


        scope.io.frequency_groups[0].line_items = []
        scope.io.frequency_groups[1].line_items = []
        scope.io.frequency_groups[0].line_items.push(scope.io.line_items[0].id)
        scope.io.frequency_groups[1].line_items.push(scope.io.line_items[1].id)
        scope.io.frequency_groups[1].line_items.push(scope.io.line_items[2].id)

        expect(scope.isAllLineItemsSelected()).toBeTruthy()


        scope.io.frequency_groups[0].line_items = []
        scope.io.frequency_groups[1].line_items=[]

        expect(scope.isAllLineItemsSelected()).toBeFalsy()

    })

    it("should return if unused groups in lists", function(){
        expect(scope.hasUnusedGroup()).toBeFalsy()

        scope.io.frequency_groups[0].line_items = []
        scope.io.frequency_groups[0].line_items = scope.io.line_items.map(function(p) { return p.id;})
        expect(scope.hasUnusedGroup()).toBeTruthy()

    })

    it('should add all line items to first group', function(){
        scope.io.frequency_groups[0].line_items = [1]
        scope.io.frequency_groups[1].line_items = [2]

        scope.allCheckCondition()
        expect(scope.io.frequency_groups[0].line_items).toEqual([])
        expect(scope.io.frequency_groups[1].line_items).toEqual([])

        scope.allCheck = true
        scope.allCheckCondition()
        expect(scope.io.frequency_groups[0].line_items).toEqual(scope.io.line_items.map(function(p) { return p.id;}))
        expect(scope.io.frequency_groups[1].line_items).toEqual([])


    })


});